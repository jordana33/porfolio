// was setting up require when I was using knockout.  set it up again latter.

console.log("got here!");
require.config({
    baseUrl: 'scripts',
    paths: {
        jquery: '//ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.3.min',
        knockout: '//ajax.aspnetcdn.com/ajax/knockout/knockout-3.1.0',
        knockoutMapping: '//cdnjs.cloudflare.com/ajax/libs/knockout.mapping/2.4.1/knockout.mapping', 
        domReady: 'lib/domReady'
    }
});
