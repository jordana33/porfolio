
var resumeApp = angular.module('resumeApp',[]);

resumeApp.filter('jobYear', function() {
  return function(year1) {
    console.log("this filter does nothing...testing");
  };
});

resumeApp.controller('portfolioCtrl', function($scope, $http){
	$http.get('data/resume.json').success(function(data) {
    	$scope.resume = data;
    	// build date range for each job
		for (var i =0; i< $scope.resume.work.length; i++){
			if($scope.resume.work[i].startDate == $scope.resume.work[i].endDate){
				$scope.resume.work[i].dateRange = $scope.resume.work[i].startDate;

			}else{
				$scope.resume.work[i].dateRange = $scope.resume.work[i].startDate + " - " + $scope.resume.work[i].endDate;
			}
		}	
	});
});
